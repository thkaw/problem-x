#include "common.h"

void list_Load_LBAData(struct linkedListData *lHData, struct linkedListData *lCData);
void list_Add_LBAData(int iAddLBA, struct linkedListData *lData);
void list_Display_AllLBAData(struct linkedListData *lData);
void list_Add_NewLBAData(int iModLBA, struct linkedListData *lData);
void list_Del_LastBlock(struct linkedListData *lData);
void list_SearchAndMove_LBAData(int iSearchLBA);
void list_SearchLBA(int iSearchLBA);

struct linkedListData
{
	struct linkedList *lHead;
	struct linkedList *lCurrent;
	struct linkedList *lPrevious;
	int DataType; //DataType->1:HotData, 0:ColdData
};

struct linkedList
{
	int iLBA;
	struct linkedList *lNext;
};
