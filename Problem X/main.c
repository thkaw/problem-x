#include "common.h"
#include "part_LinkedList.h"
#include "part_File.h"

#define defaultHotDataSize 2
#define defaultColdDataSize 4

int main()
{
	/*int iHotDataListSize = defaultHotDataSize;
	int iColdDataListSize = defaultColdDataSize;*/

	printf("=========================================================\n");
	printf("=============Start Problem X Main Program...=============\n");
	printf("=========================================================\n");
	printf("===============%s %s Ver1.2===============\n\n\n", __DATE__, __TIME__);


	int HC[2] = { 0 };
	long SetData[MaxInputSetReadDataSize] = { 0 };
	//long ReadData[MaxInputSetReadDataSize] = { 0 };
	long QueryData[MaxInputQueryDataSize] = { 0 };
	file_ReadData(HC, SetData, QueryData);
	//file_ReadData(HC, SetData, ReadData, QueryData);
	int iHotDataListSize = HC[0];
	int iColdDataListSize = HC[1];



	//final input
	//手動輸入H跟C SIZE
	/*

		char sInputTemp[64] = { 0 };

		printf("Please input HotData List Size(Default 2, 1<=H<=5000):");

		if (scanf("%[^\n]", sInputTemp))
		{
		iHotDataListSize = atoi(sInputTemp);
		if (iHotDataListSize > 5000)
		{
		printf("Input overange!, use default 10.\n");
		iHotDataListSize = defaultHotDataSize;
		}

		}
		getchar();

		printf("Please input ColdData List Size(Default 4, 1<=C<=10000):");

		if (scanf("%[^\n]", sInputTemp))
		{
		iColdDataListSize = atoi(sInputTemp);
		if (iColdDataListSize > 10000)
		{
		printf("Input overange!, use default 20.\n");
		iColdDataListSize = defaultColdDataSize;
		}
		}
		getchar();

		*/
	//如果SIZE不合法，則用預設的H跟C的SIZE
	if (iColdDataListSize < iHotDataListSize)
	{
		printf("Cold size less than Hot size, use default now.\n");
		iHotDataListSize = defaultHotDataSize;
		iColdDataListSize = defaultColdDataSize;
	}

	printf("->>>HotData List Size is:%d\n->>>ColdData List Size is:%d\n\n", iHotDataListSize, iColdDataListSize);


	//建立HotData主要結構
	struct linkedListData *lldHotData = (struct linkedListData *)malloc(sizeof(struct linkedListData));;
	lldHotData->lCurrent = NULL;
	lldHotData->lHead = NULL;
	lldHotData->lPrevious = NULL;
	lldHotData->DataType = 1;//HotData

	//初始化空白LBADATA CACHE
	for (int i = 0; i < iHotDataListSize; i++)
	{
		list_Add_LBAData(NULL, lldHotData);
	}

	//建立HotData主要結構
	struct linkedListData *lldColdData = (struct linkedListData *)malloc(sizeof(struct linkedListData));;
	lldColdData->lCurrent = NULL;
	lldColdData->lHead = NULL;
	lldColdData->lPrevious = NULL;
	lldColdData->DataType = 0;//ColdData

	//loading datastructre
	list_Load_LBAData(lldHotData, lldColdData);

	//初始化空白LBADATA CACHE
	for (int i = 0; i < iColdDataListSize; i++)
	{
		list_Add_LBAData(NULL, lldColdData);
	}

	//開始進行LBA DATA置入
	for (int i = 0; i <= MaxInputSetReadDataSize; i++)
	{
		if (SetData[i] == 0)
		{
			break;
		}
		list_SearchAndMove_LBAData(SetData[i]);
	}

	//顯示Linkedlist詳細的資料內容(註解掉可以獲得較好的效能)
	list_Display_AllLBAData(lldHotData);

	list_Display_AllLBAData(lldColdData);

	//輸出最終結果
	printf("\n===========Start Output===========\n");


	for (int i = 0; i <= MaxInputQueryDataSize; i++)
	{
		if (QueryData[i] == 0)
		{
			break;
		}
		list_SearchLBA(QueryData[i]);
	}

	printf("============End Output============\n");

	system("pause");

	return(0);

}