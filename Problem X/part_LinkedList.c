#include "part_LinkedList.h"

struct linkedListData *localHotData;
struct linkedListData *localColdData;

void list_Load_LBAData(struct linkedListData *lHData, struct linkedListData *lCData)
{
	localHotData = lHData;
	localColdData = lCData;
}

//這裡拿來純粹初始化LBA的空間
void list_Add_LBAData(int iAddLBA, struct linkedListData *lData)
{
	/*char sDataType[32] = "[init HotDataListLBA]->>>";
	if (lData->DataType == 0)
	{
	strcpy(sDataType, "[init ColdDataListLBA]->>>");
	}

	printf("%s%d\n", sDataType, iAddLBA);*/

	lData->lCurrent = (struct linkedList *)malloc(sizeof(struct linkedList));
	lData->lCurrent->lNext = NULL;
	lData->lCurrent->iLBA = iAddLBA;

	if (lData->lHead == NULL)
	{
		lData->lHead = lData->lCurrent;
	}
	else
	{
		lData->lPrevious->lNext = lData->lCurrent;
	}

	lData->lPrevious = lData->lCurrent;

}

void list_Display_AllLBAData(struct linkedListData *lData)
{
	char sDataType[32] = "HotDataListLBA->>>";
	if (lData->DataType == 0)
	{
		strcpy(sDataType, "ColdDataListLBA->>>");
	}

	lData->lCurrent = lData->lHead;
	while (lData->lCurrent != NULL)
	{
		printf("%s%d\n", sDataType, lData->lCurrent->iLBA);
		lData->lCurrent = lData->lCurrent->lNext;
	}
}

//移除最後一個block，並且於list前端插入新的block
void list_Add_NewLBAData(int iModLBA, struct linkedListData *lData)
{
	list_Del_LastBlock(lData);
	lData->lCurrent = (struct linkedList *)malloc(sizeof(struct linkedList));
	lData->lCurrent->lNext = lData->lHead;
	lData->lCurrent->iLBA = iModLBA;

	lData->lHead = lData->lCurrent;

}

//HotToCold的時候，ColdDataHead不需要做preRoute或者補block的行為(cold to hot才需要)
int iTransferToCold = 0;
//將hotdata最後面的資料，擠回cold data的最前方
void list_Move_HotLBADataToCold(int iModLBA, struct linkedListData *lData)
{
	lData->lCurrent = (struct linkedList *)malloc(sizeof(struct linkedList));
	lData->lCurrent->lNext = lData->lHead;
	lData->lCurrent->iLBA = iModLBA;

	lData->lHead = lData->lCurrent;
	iTransferToCold = 1;
}


void list_Del_LastBlock(struct linkedListData *lData)
{
	lData->lCurrent = lData->lHead;
	while (lData->lCurrent != NULL)
	{
		if (lData->lCurrent->lNext == NULL)//最後一個節點
		{
			lData->lPrevious->lNext = NULL;

			if (lData->DataType == 0){
				free(lData->lCurrent);
				break;
			}
			else if (lData->DataType == 1 && lData->lCurrent->iLBA != 0)//hotdata
			{
				list_Move_HotLBADataToCold(lData->lCurrent->iLBA, localColdData);
			}
		}

		lData->lPrevious = lData->lCurrent;
		lData->lCurrent = lData->lCurrent->lNext;
	}

}

//主要的搬移判斷程序
void list_SearchAndMove_LBAData(int iSearchLBA)
{
	int bHaveFound = 0;//有沒有在現有的串列中找到

	//先去hot data找是否有相同內容的資料，有的話則把她往hot data的前面移動
	localHotData->lCurrent = localHotData->lHead;
	while (localHotData->lCurrent != NULL&&!bHaveFound)
	{
		if (localHotData->lCurrent->iLBA == iSearchLBA)//在hot data中找到相同的資料，搬移到hot data最前方
		{

			//如果再hotdata找到的相同資料已經在最前方，則沒有必要做任何事情
			if (localHotData->lCurrent != localHotData->lHead)
			{
				localHotData->lPrevious->lNext = localHotData->lCurrent->lNext;
				localHotData->lCurrent->lNext = localHotData->lHead;
				localHotData->lHead = localHotData->lCurrent;
			}


			bHaveFound = 1;
			break;
		}

		localHotData->lPrevious = localHotData->lCurrent;
		localHotData->lCurrent = localHotData->lCurrent->lNext;
	}

	//如果hot data沒有，則去cold data找是否有相同內容的資料，有的話則把她往hot data的前面移動
	localColdData->lCurrent = localColdData->lHead;
	while (localColdData->lCurrent != NULL&&!bHaveFound)
	{
		if (localColdData->lCurrent->iLBA == iSearchLBA)//在cold data中找到相同的資料，搬移到hot data最前方
		{
			//todo關鍵貌似在這一區，cold data有bug ->>solve

			//如果找到的BLOCK在串列中間
			if (localColdData->lCurrent != localColdData->lHead)
			{
				localColdData->lPrevious->lNext = localColdData->lCurrent->lNext;
			}
			//如果找到的BLOCK在串列最前面
			else if (localColdData->lCurrent == localColdData->lHead)
			{
				localColdData->lHead = localColdData->lCurrent->lNext;
			}

			//在cold data中找到相同的資料，搬移到hot data~~~
			list_Add_NewLBAData(localColdData->lCurrent->iLBA, localHotData);


			//HotToCold的時候，ColdDataHead不需要做補block的行為(cold to hot抽離cold block才需要)
			if (!iTransferToCold){

				/*	if (localColdData->lCurrent == localColdData->lHead)
					{
					localColdData->lHead = localColdData->lCurrent->lNext;
					localColdData->lHead->lNext = localColdData->lPrevious;
					}
					else
					{
					localColdData->lPrevious->lNext = localColdData->lCurrent->lNext;
					}*/


				free(localColdData->lCurrent);

				//補尾
				localColdData->lCurrent = localColdData->lHead;
				while (localColdData->lCurrent != NULL)
				{
					if (localColdData->lCurrent->lNext == NULL)//最後一個節點
					{
						localColdData->lCurrent->lNext = (struct linkedList *)malloc(sizeof(struct linkedList));
						localColdData->lCurrent->lNext->iLBA = NULL;
						localColdData->lCurrent->lNext->lNext = NULL;
						break;
					}

					//lCData->lPrevious = lCData->lCurrent;
					localColdData->lCurrent = localColdData->lCurrent->lNext;
				}

			}

			iTransferToCold = 0;

			bHaveFound = 1;
			break;
		}

		localColdData->lPrevious = localColdData->lCurrent;
		localColdData->lCurrent = localColdData->lCurrent->lNext;
	}

	if (!bHaveFound)
	{
		list_Add_NewLBAData(iSearchLBA, localColdData);
	}

}

void list_SearchLBA(int iSearchLBA)
{
	int bHaveFound = 0;//有沒有在現有的串列中找到
	localColdData->lCurrent = localColdData->lHead;

	//先找cold data有沒有要找的內容
	while (localColdData->lCurrent != NULL)
	{
		if (localColdData->lCurrent->iLBA == iSearchLBA)
		{

			printf("COLD\n");

			bHaveFound = 1;
			break;
		}

		localColdData->lPrevious = localColdData->lCurrent;
		localColdData->lCurrent = localColdData->lCurrent->lNext;
	}


	//如果cold data沒有，則去hot data找是否有相同內容的資料
	localHotData->lCurrent = localHotData->lHead;
	while (localHotData->lCurrent != NULL&&!bHaveFound)
	{
		if (localHotData->lCurrent->iLBA == iSearchLBA)
		{

			printf("HOT\n");

			bHaveFound = 1;
			break;
		}

		localHotData->lPrevious = localHotData->lCurrent;
		localHotData->lCurrent = localHotData->lCurrent->lNext;
	}

	//如果再hot&cold data都沒找到要搜尋的目標，則為cold
	if (!bHaveFound)
	{
		printf("COLD\n");
	}

}