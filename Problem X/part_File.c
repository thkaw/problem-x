#include "part_File.h"

//讀檔用  //long *retReadData[MaxInputSetReadDataSize], 
int file_ReadData(int *retHC[2], long *retSetData[MaxInputSetReadDataSize], long *retQueryData[MaxInputQueryDataSize])
{
	char scanfTemp[64] = { 0 };
	char c1[16] = { 0 };
	char c2[16] = { 0 };

	int iSetDataArrayIndex = 0;
	//int iReadataArrayIndex = 0;
	int iQueryDataArrayIndex = 0;


	FILE *fp_Data_read = fopen(InputFilePath, "r");

	int i, col = MaxInputSetReadDataSize;
	int checkOutOfRange = 0;

	printf("======Start Read %s=======\n", InputFilePath);

	for (i = 1; i <= col; i++)
	{
		checkOutOfRange = fscanf(fp_Data_read, "%[^\n]\n", scanfTemp);
		if (checkOutOfRange <= 0)
		{
			//避免fscanf掃描到沒有內容的行數還繼續run浪費資源
			break;
		}
		if (i == 1)
		{
			//printf("%s\n", scanfTemp); //註解掉獲得比較好的效能
			sscanf(scanfTemp, "%s %s", c1, c2);
			retHC[0] = atoi(c1);
			retHC[1] = atoi(c2);

		}
		else
		{
			//printf("%s\n", scanfTemp); //註解掉獲得比較好的效能
			sscanf(scanfTemp, "%s", c1);//先讀取第一個欄位，避免沒有第二個欄位可能crash.

			char *loc;
			loc = strstr(c1, "W");
			if (loc == NULL)//為查詢LBA資料熱度
			{
				loc = strstr(c1, "R");//如果不是R也不是W，則為QUERY
				if (loc == NULL)
				{
					retQueryData[iQueryDataArrayIndex] = atoi(c1);
					iQueryDataArrayIndex++;
				}
				else//為R指令，代表後面有LBA要讀取
				{
				/*	sscanf(scanfTemp, "%s %s", c1, c2);
					retReadData[iReadataArrayIndex] = atoi(c2);
					iReadataArrayIndex++;*/
				}
			}
			else//為W指令，代表後面有LBA要寫入
			{
				sscanf(scanfTemp, "%s %s", c1, c2);
				retSetData[iSetDataArrayIndex] = atoi(c2);
				iSetDataArrayIndex++;
			}




		}
		//printf("[FILE]->>>checkOutOfRange is:%d\n", checkOutOfRange);
	}
	printf("=======End Read %s========\n\n", InputFilePath);

	fclose(fp_Data_read);

	return 0;

}