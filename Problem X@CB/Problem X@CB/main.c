#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define InputFilePath "testdata.in"

#define MaxInputSetReadDataSize 60000
#define MaxInputQueryDataSize 128

#define defaultHotDataSize 2
#define defaultColdDataSize 4

struct linkedListData
{
	struct linkedList *lHead;
	struct linkedList *lCurrent;
	struct linkedList *lPrevious;
	int DataType; //DataType->1:HotData, 0:ColdData
};

struct linkedList
{
	int iLBA;
	struct linkedList *lNext;
};

//讀檔用  //long *retReadData[MaxInputSetReadDataSize],
int file_ReadData(int *retHC[2], long *retSetData[MaxInputSetReadDataSize], long *retQueryData[MaxInputQueryDataSize])
{
	char scanfTemp[64] = { 0 };
	char c1[16] = { 0 };
	char c2[16] = { 0 };

	int iSetDataArrayIndex = 0;
	//int iReadataArrayIndex = 0;
	int iQueryDataArrayIndex = 0;


	FILE *fp_Data_read = fopen(InputFilePath, "r");

	int i, col = MaxInputSetReadDataSize;
	int checkOutOfRange = 0;

	printf("======Start Read %s=======\n", InputFilePath);

	for (i = 1; i <= col; i++)
	{
		checkOutOfRange = fscanf(fp_Data_read, "%[^\n]\n", scanfTemp);
		if (checkOutOfRange <= 0)
		{
			//避免fscanf掃描到沒有內容的行數還繼續run浪費資源
			break;
		}
		if (i == 1)
		{
			//printf("%s\n", scanfTemp); //註解掉獲得比較好的效能
			sscanf(scanfTemp, "%s %s", c1, c2);
			retHC[0] = atoi(c1);
			retHC[1] = atoi(c2);

		}
		else
		{
			//printf("%s\n", scanfTemp); //註解掉獲得比較好的效能
			sscanf(scanfTemp, "%s", c1);//先讀取第一個欄位，避免沒有第二個欄位可能crash.

			char *loc;
			loc = strstr(c1, "W");
			if (loc == NULL)//為查詢LBA資料熱度
			{
				loc = strstr(c1, "R");//如果不是R也不是W，則為QUERY
				if (loc == NULL)
				{
					retQueryData[iQueryDataArrayIndex] = atoi(c1);
					iQueryDataArrayIndex++;
				}
				else//為R指令，代表後面有LBA要讀取
				{
				/*	sscanf(scanfTemp, "%s %s", c1, c2);
					retReadData[iReadataArrayIndex] = atoi(c2);
					iReadataArrayIndex++;*/
				}
			}
			else//為W指令，代表後面有LBA要寫入
			{
				sscanf(scanfTemp, "%s %s", c1, c2);
				retSetData[iSetDataArrayIndex] = atoi(c2);
				iSetDataArrayIndex++;
			}

		}
		//printf("[FILE]->>>checkOutOfRange is:%d\n", checkOutOfRange);
	}
	printf("=======End Read %s========\n\n", InputFilePath);

	fclose(fp_Data_read);

	return 0;

}


struct linkedListData *localHotData;
struct linkedListData *localColdData;

void list_Load_LBAData(struct linkedListData *lHData, struct linkedListData *lCData)
{
	localHotData = lHData;
	localColdData = lCData;
}

//這裡拿來純粹初始化LBA的空間
void list_Add_LBAData(int iAddLBA, struct linkedListData *lData)
{
	/*char sDataType[32] = "[init HotDataListLBA]->>>";
	if (lData->DataType == 0)
	{
	strcpy(sDataType, "[init ColdDataListLBA]->>>");
	}

	printf("%s%d\n", sDataType, iAddLBA);*/

	lData->lCurrent = (struct linkedList *)malloc(sizeof(struct linkedList));
	lData->lCurrent->lNext = NULL;
	lData->lCurrent->iLBA = iAddLBA;

	if (lData->lHead == NULL)
	{
		lData->lHead = lData->lCurrent;
	}
	else
	{
		lData->lPrevious->lNext = lData->lCurrent;
	}

	lData->lPrevious = lData->lCurrent;

}

void list_Display_AllLBAData(struct linkedListData *lData)
{
	char sDataType[32] = "HotDataListLBA->>>";
	if (lData->DataType == 0)
	{
		strcpy(sDataType, "ColdDataListLBA->>>");
	}

	lData->lCurrent = lData->lHead;
	while (lData->lCurrent != NULL)
	{
		printf("%s%d\n", sDataType, lData->lCurrent->iLBA);
		lData->lCurrent = lData->lCurrent->lNext;
	}
}

//移除最後一個block，並且於list前端插入新的block
void list_Add_NewLBAData(int iModLBA, struct linkedListData *lData)
{
	list_Del_LastBlock(lData);
	lData->lCurrent = (struct linkedList *)malloc(sizeof(struct linkedList));
	lData->lCurrent->lNext = lData->lHead;
	lData->lCurrent->iLBA = iModLBA;

	lData->lHead = lData->lCurrent;

}

//HotToCold的時候，ColdDataHead不需要做preRoute或者補block的行為(cold to hot才需要)
int iTransferToCold = 0;
//將hotdata最後面的資料，擠回cold data的最前方
void list_Move_HotLBADataToCold(int iModLBA, struct linkedListData *lData)
{
	lData->lCurrent = (struct linkedList *)malloc(sizeof(struct linkedList));
	lData->lCurrent->lNext = lData->lHead;
	lData->lCurrent->iLBA = iModLBA;

	lData->lHead = lData->lCurrent;
	iTransferToCold = 1;
}


void list_Del_LastBlock(struct linkedListData *lData)
{
	lData->lCurrent = lData->lHead;
	while (lData->lCurrent != NULL)
	{
		if (lData->lCurrent->lNext == NULL)//最後一個節點
		{
			lData->lPrevious->lNext = NULL;

			if (lData->DataType == 0){
				free(lData->lCurrent);
				break;
			}
			else if (lData->DataType == 1 && lData->lCurrent->iLBA != 0)//hotdata
			{
				list_Move_HotLBADataToCold(lData->lCurrent->iLBA, localColdData);
			}
		}

		lData->lPrevious = lData->lCurrent;
		lData->lCurrent = lData->lCurrent->lNext;
	}

}

//主要的搬移判斷程序
void list_SearchAndMove_LBAData(int iSearchLBA)
{
	int bHaveFound = 0;//有沒有在現有的串列中找到

	//先去hot data找是否有相同內容的資料，有的話則把她往hot data的前面移動
	localHotData->lCurrent = localHotData->lHead;
	while (localHotData->lCurrent != NULL&&!bHaveFound)
	{
		if (localHotData->lCurrent->iLBA == iSearchLBA)//在hot data中找到相同的資料，搬移到hot data最前方
		{

			//如果再hotdata找到的相同資料已經在最前方，則沒有必要做任何事情
			if (localHotData->lCurrent != localHotData->lHead)
			{
				localHotData->lPrevious->lNext = localHotData->lCurrent->lNext;
				localHotData->lCurrent->lNext = localHotData->lHead;
				localHotData->lHead = localHotData->lCurrent;
			}


			bHaveFound = 1;
			break;
		}

		localHotData->lPrevious = localHotData->lCurrent;
		localHotData->lCurrent = localHotData->lCurrent->lNext;
	}

	//如果hot data沒有，則去cold data找是否有相同內容的資料，有的話則把她往hot data的前面移動
	localColdData->lCurrent = localColdData->lHead;
	while (localColdData->lCurrent != NULL&&!bHaveFound)
	{
		if (localColdData->lCurrent->iLBA == iSearchLBA)//在cold data中找到相同的資料，搬移到hot data最前方
		{
			//todo關鍵貌似在這一區，cold data有bug ->>solve

			//如果找到的BLOCK在串列中間
			if (localColdData->lCurrent != localColdData->lHead)
			{
				localColdData->lPrevious->lNext = localColdData->lCurrent->lNext;
			}
			//如果找到的BLOCK在串列最前面
			else if (localColdData->lCurrent == localColdData->lHead)
			{
				localColdData->lHead = localColdData->lCurrent->lNext;
			}

			//在cold data中找到相同的資料，搬移到hot data~~~
			list_Add_NewLBAData(localColdData->lCurrent->iLBA, localHotData);


			//HotToCold的時候，ColdDataHead不需要做補block的行為(cold to hot抽離cold block才需要)
			if (!iTransferToCold){

				/*	if (localColdData->lCurrent == localColdData->lHead)
					{
					localColdData->lHead = localColdData->lCurrent->lNext;
					localColdData->lHead->lNext = localColdData->lPrevious;
					}
					else
					{
					localColdData->lPrevious->lNext = localColdData->lCurrent->lNext;
					}*/


				free(localColdData->lCurrent);

				//補尾
				localColdData->lCurrent = localColdData->lHead;
				while (localColdData->lCurrent != NULL)
				{
					if (localColdData->lCurrent->lNext == NULL)//最後一個節點
					{
						localColdData->lCurrent->lNext = (struct linkedList *)malloc(sizeof(struct linkedList));
						localColdData->lCurrent->lNext->iLBA = NULL;
						localColdData->lCurrent->lNext->lNext = NULL;
						break;
					}

					//lCData->lPrevious = lCData->lCurrent;
					localColdData->lCurrent = localColdData->lCurrent->lNext;
				}

			}

			iTransferToCold = 0;

			bHaveFound = 1;
			break;
		}

		localColdData->lPrevious = localColdData->lCurrent;
		localColdData->lCurrent = localColdData->lCurrent->lNext;
	}

	if (!bHaveFound)
	{
		list_Add_NewLBAData(iSearchLBA, localColdData);
	}

}

void list_SearchLBA(int iSearchLBA)
{
	int bHaveFound = 0;//有沒有在現有的串列中找到
	localColdData->lCurrent = localColdData->lHead;

	//先找cold data有沒有要找的內容
	while (localColdData->lCurrent != NULL)
	{
		if (localColdData->lCurrent->iLBA == iSearchLBA)
		{

			printf("COLD\n");

			bHaveFound = 1;
			break;
		}

		localColdData->lPrevious = localColdData->lCurrent;
		localColdData->lCurrent = localColdData->lCurrent->lNext;
	}


	//如果cold data沒有，則去hot data找是否有相同內容的資料
	localHotData->lCurrent = localHotData->lHead;
	while (localHotData->lCurrent != NULL&&!bHaveFound)
	{
		if (localHotData->lCurrent->iLBA == iSearchLBA)
		{

			printf("HOT\n");

			bHaveFound = 1;
			break;
		}

		localHotData->lPrevious = localHotData->lCurrent;
		localHotData->lCurrent = localHotData->lCurrent->lNext;
	}

	//如果再hot&cold data都沒找到要搜尋的目標，則為cold
	if (!bHaveFound)
	{
		printf("COLD\n");
	}

}

int main()
{
	/*int iHotDataListSize = defaultHotDataSize;
	int iColdDataListSize = defaultColdDataSize;*/

	printf("=========================================================\n");
	printf("=============Start Problem X Main Program...=============\n");
	printf("=========================================================\n");
	printf("===============%s %s Ver1.2===============\n\n\n", __DATE__, __TIME__);


	int HC[2] = { 0 };
	long SetData[MaxInputSetReadDataSize] = { 0 };
	//long ReadData[MaxInputSetReadDataSize] = { 0 };
	long QueryData[MaxInputQueryDataSize] = { 0 };
	file_ReadData(HC, SetData, QueryData);
	//file_ReadData(HC, SetData, ReadData, QueryData);
	int iHotDataListSize = HC[0];
	int iColdDataListSize = HC[1];



	//final input
	//手動輸入H跟C SIZE
	/*

		char sInputTemp[64] = { 0 };

		printf("Please input HotData List Size(Default 2, 1<=H<=5000):");

		if (scanf("%[^\n]", sInputTemp))
		{
		iHotDataListSize = atoi(sInputTemp);
		if (iHotDataListSize > 5000)
		{
		printf("Input overange!, use default 10.\n");
		iHotDataListSize = defaultHotDataSize;
		}

		}
		getchar();

		printf("Please input ColdData List Size(Default 4, 1<=C<=10000):");

		if (scanf("%[^\n]", sInputTemp))
		{
		iColdDataListSize = atoi(sInputTemp);
		if (iColdDataListSize > 10000)
		{
		printf("Input overange!, use default 20.\n");
		iColdDataListSize = defaultColdDataSize;
		}
		}
		getchar();

		*/
	//如果SIZE不合法，則用預設的H跟C的SIZE
	if (iColdDataListSize < iHotDataListSize)
	{
		printf("Cold size less than Hot size, use default now.\n");
		iHotDataListSize = defaultHotDataSize;
		iColdDataListSize = defaultColdDataSize;
	}

	printf("->>>HotData List Size is:%d\n->>>ColdData List Size is:%d\n\n", iHotDataListSize, iColdDataListSize);


	//建立HotData主要結構
	struct linkedListData *lldHotData = (struct linkedListData *)malloc(sizeof(struct linkedListData));;
	lldHotData->lCurrent = NULL;
	lldHotData->lHead = NULL;
	lldHotData->lPrevious = NULL;
	lldHotData->DataType = 1;//HotData

	//初始化空白LBADATA CACHE
	for (int i = 0; i < iHotDataListSize; i++)
	{
		list_Add_LBAData(NULL, lldHotData);
	}

	//建立HotData主要結構
	struct linkedListData *lldColdData = (struct linkedListData *)malloc(sizeof(struct linkedListData));;
	lldColdData->lCurrent = NULL;
	lldColdData->lHead = NULL;
	lldColdData->lPrevious = NULL;
	lldColdData->DataType = 0;//ColdData

	//loading datastructre
	list_Load_LBAData(lldHotData, lldColdData);

	//初始化空白LBADATA CACHE
	for (int i = 0; i < iColdDataListSize; i++)
	{
		list_Add_LBAData(NULL, lldColdData);
	}

	//開始進行LBA DATA置入
	for (int i = 0; i <= MaxInputSetReadDataSize; i++)
	{
		if (SetData[i] == 0)
		{
			break;
		}
		list_SearchAndMove_LBAData(SetData[i]);
	}

	//顯示Linkedlist詳細的資料內容(註解掉可以獲得較好的效能)
	list_Display_AllLBAData(lldHotData);

	list_Display_AllLBAData(lldColdData);

	//輸出最終結果
	printf("\n===========Start Output===========\n");


	for (int i = 0; i <= MaxInputQueryDataSize; i++)
	{
		if (QueryData[i] == 0)
		{
			break;
		}
		list_SearchLBA(QueryData[i]);
	}

	printf("============End Output============\n");

	system("pause");

	return(0);

}
